<?php
/**
 * Created by PhpStorm.
 * User: nathan
 * Date: 12/6/18
 * Time: 11:07 AM
 */

/*  */
require_once '../../../../autoload.php';
require_once '../../src/GladeCore.php';

/*  GET REQUEST PARAMS  */
$card = [];
$card['card_no'] = isset($_POST['card_number']) ? $_POST['card_number'] : '1234567812345678';
$card['expiry_month'] = isset($_POST['exp_month']) ? $_POST['exp_month'] : '01';
$card['expiry_year'] = isset($_POST['exp_year']) ? $_POST['exp_year'] : '19';
$card['ccv'] = isset($_POST['cvc']) ? $_POST['cvc'] : '123';
$card['pin'] = isset($_POST['pin']) ? $_POST['pin'] : '1234';

$user = isset($_POST['user']) ? $_POST['user'] : '[]';
$merchant = isset($_POST['merchant']) ? $_POST['merchant'] : '[]';
$amount = isset($_POST['amount']) ? $_POST['amount'] : '[]';
$gateway_mode = isset($_POST['mode']) ? $_POST['mode'] : 'live';

$user = unserialize($user);
$merchant = unserialize($merchant);

/*  */
try {
    $paymentHandle = new \App\GladeCore();
    $paymentHandle->setInitiationParameters($merchant, $amount, $card, $user, $gateway_mode, 'NG');

    if($paymentHandle->initiatePayment())
    {
        echo json_encode($paymentHandle->getResponse());
    }else {
        echo '{"status": "100", "message": "Could not generate OTP"}';
    }
}catch (Exception $e)
{
    echo '{"status": "000", "message": "'.$e->getMessage().'"}';
}
