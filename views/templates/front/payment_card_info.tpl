{*


*}
<form action="{$action}" method="post" name="gladepay_form">
    <div id="form-message"></div>
    <div id="card_info">
        <div class="row">
            <input type="hidden" name="mode" value="{$mode}">
            <input type="hidden" name="user" value="{$user}">
            <input type="hidden" name="merchant" value="{$merchant}">
            <input type="hidden" name="amount" value="{$amount}">
            <input type="hidden" name="currency" value="{$currency}">
            <input type="hidden" name="reference" value="">
        </div>
        <div class="row">
            <input type="text" name="card_number" placeholder="Card Number" autocomplete="off" value="" class="input-100">
        </div>
        <div class="row">
            <select id="month" name="card_expiry_month" class="gladepay_select">
                {foreach from=$months item=month}
                    <option value="{$month}">{$month}</option>
                {/foreach}
            </select>
            <select id="year" name="card_expiry_year" class="gladepay_select">
                {foreach from=$years item=year}
                    <option value="{$year}">{$year}</option>
                {/foreach}
            </select>
        </div>
        <div class="row">
            <input type="text" name="card_cvc" placeholder="CVC" autocomplete="off" value="" class="input-50">
            <input type="password" name="card_pin" placeholder="PIN" autocomplete="off" value="" class="input-50">
        </div>
        <div class="row">
            <input type="button" id="get_otp_btn" value="Generate OTP">
        </div>
    </div>
    <div id="card_otp">
        <div class="row">
            <input type="text" size="20" autocomplete="off" name="otp" placeholder="Enter OTP" class="input-100">
        </div>
        <div class="row">
            <input type="button" id="resend_otp_btn" value="Resend OTP">
        </div>
    </div>
    <div id="gp_loader">
        <img src="{$loader}" class="gp_img">
    </div>
</form>