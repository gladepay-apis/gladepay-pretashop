<?php

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined("_PS_VERSION_")) exit;

class GladePay extends PaymentModule
{

    public function __construct()
    {
        $this->name = "gladepay";
        $this->tab = "payments_gateways";
        $this->version = "1.0.0";
        $this->author = "GladePay Inc";
        $this->need_instance = 0;
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => _PS_VERSION_];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('GladePay');
        $this->description = $this->l('GladePay framework for making payments.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        //  Set module name for easy referencing
        Configuration::updateValue('GLADEPAY_MOD_NAME', $this->name);

        if (!Configuration::get('GLADEPAY_MOD_NAME'))
            $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        $returnValue = false;

        if (function_exists('curl_init')
            && parent::install()
            && $this->registerHook('Header')
            && $this->registerHook('paymentOptions')
            && $this->registerHook('paymentReturn'))
        {
            Configuration::updateValue('GLADEPAY_MERCHANT_ID', '');
            Configuration::updateValue('GLADEPAY_MERCHANT_KEY', '');
            Configuration::updateValue('GLADEPAY_DEFAULT_CURRENCY', 'NGN');

            $returnValue = true;
        }

        return $returnValue;
    }

    public function uninstall()
    {
        $returnValue = false;

        if (Configuration::deleteByName('GLADEPAY_MERCHANT_ID') &&
            Configuration::deleteByName('GLADEPAY_MERCHANT_KEY') && $this->unregisterHook('Header')
            && $this->unregisterHook('paymentOptions')
            && $this->unregisterHook('paymentReturn')
            && parent::uninstall())
        {
            $returnValue = true;
        }

        return $returnValue;
    }

    public function hookHeader()
    {
        $this->context->controller->addCSS(($this->_path).'views/css/pay_style.css', 'all');
        $this->context->controller->addJS(($this->_path).'views/scripts/jquery-3.3.1.js');
        $this->context->controller->addJS(($this->_path).'views/scripts/System.js');
        $this->context->controller->addJS(($this->_path).'views/scripts/card.js');
        $this->context->controller->addJS(($this->_path).'views/scripts/cart.js');
        $this->context->controller->addJS(($this->_path).'views/scripts/payment.js');
        $this->context->controller->addJS(($this->_path).'views/scripts/gladepay.js');
    }

    public function hookPaymentOptions($params)
    {
        if(!$this->active) {
            return;
        }

        $paymentOptions = [];

        if (Configuration::get('GLADEPAY_MERCHANT_ID') && Configuration::get('GLADEPAY_MERCHANT_KEY'))
        {
            $opt = new PaymentOption();
            $opt
                ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/views/images/gladepay.jpg'))
                ->setForm($this->generateCardInfoForm());

            $paymentOptions[] = $opt;
        }

        return $paymentOptions;
    }

    public function getContent()
    {
        $output = null;

        if(Tools::isSubmit('submit' . $this->name))
        {
            $merchant_id = Tools::getValue('GP_MERCHANT_ID');
            $merchant_key = Tools::getValue('GP_MERCHANT_KEY');
            $gateway_mode = Tools::getValue('GP_GATEWAY_MODE');

            if (!$merchant_id || empty($merchant_id || !Validate::isGeneric($merchant_id))
                || !$merchant_key || empty($merchant_key || !Validate::isGeneric($merchant_key))
                || empty($gateway_mode))
            {
                $output .$this->displayError($this->l("Invalid configuration values"));
            } else
            {
                Configuration::updateValue('GLADEPAY_MERCHANT_ID', $merchant_id);
                Configuration::updateValue('GLADEPAY_MERCHANT_KEY', $merchant_key);
                Configuration::updateValue('GLADEPAY_GATEWAY_MODE', $gateway_mode);
                $output .$this->displayConfirmation($this->l('Settings Updated.'));
            }
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        $default_language = (int) Configuration::get('PS_LANG_DEFAULT');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Merchant ID'),
                    'name' => 'GP_MERCHANT_ID',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'password',
                    'label' => $this->l('Merchant Key'),
                    'name' => 'GP_MERCHANT_KEY',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Choose a Mode'),
                    'name' => 'GP_GATEWAY_MODE',
                    'required' => true,
                    'options' => array(
                        'query' => $mode = array(
                            array(
                                'mode_id' => 'test',
                                'mode_name' => 'TEST'
                            ),
                            array(
                                'mode_id' => 'live',
                                'mode_name' => 'LIVE'
                            )
                        ),
                        'id' => 'mode_id',
                        'name' => 'mode_name',
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );


        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $default_language;
        $helper->allow_employee_form_lang = $default_language;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                        '&token='.Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        // Load default value
        $helper->fields_value['GP_MERCHANT_ID'] = '';
        $helper->fields_value['GP_MERCHANT_KEY'] = '';

        return $helper->generateForm($fields_form);
    }

    public function getCustomerInfo()
    {
        $customer = $this->context->customer;
        $customerInfo = [];
        $customerInfo['fingerprint'] = $customer->id;
        $customerInfo['firstname'] = $customer->firstname;
        $customerInfo['lastname'] = $customer->lastname;
        $customerInfo['email'] = $customer->email;
        $customerInfo['ip'] = $_SERVER['REMOTE_ADDR'];

        return $customerInfo;
    }

    public function getMerchantInfo()
    {
        $merchant = [];
        $merchant['id'] = Configuration::get('GLADEPAY_MERCHANT_ID');
        $merchant['key'] = Configuration::get('GLADEPAY_MERCHANT_KEY');

        return $merchant;
    }
    public function generateCardInfoForm()
    {
        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            $months[] = sprintf("%02d", $i);
        }

        $years = [];
        for ($i = 0; $i <= 10; $i++) {
            $years[] = date('Y', strtotime('+'.$i.' years'));
        }

        $this->context->smarty->assign([
            'loader' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/views/images/loader.gif'),
            'action' => $this->context->link->getModuleLink($this->name, 'payment', array(), true),
            'mode' => Configuration::get('GLADEPAY_GATEWAY_MODE'),
            'months' => $months,
            'years' => $years,
            'merchant' => serialize($this->getMerchantInfo()),
            'user' => serialize($this->getCustomerInfo()),
            'amount' => $this->context->cart->getOrderTotal(),
            'currency' => Configuration::get('GLADEPAY_DEFAULT_CURRENCY')
        ]);

        return $this->context->smarty->fetch('module:gladepay/views/templates/front/payment_card_info.tpl');
    }
}