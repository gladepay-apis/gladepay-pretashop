<?php

    require_once './GladeCore.php';

    $merchant = [
        "id" => "GP0000001",
        "key" => "123456789"
    ];

    $card = [
        "card_no" => "5438898014560229",
        "expiry_month" => "09",
        "expiry_year" => "19",
        "ccv" => "789",
        "pin" =>"3310"
    ];

    $user = [
        "firstname" =>"John",
        "lastname" =>"Doe",
        "email" =>"hello@example.com",
        "ip" => "192.168.33.10",
        "fingerprint" => "cccvxbxbxb"
    ];

    $glade = new \App\GladeCore();
    $glade->setInitiationParameters($merchant, 1000, $card, $user, 'NG');

    $glade->initiatePayment();
    $glade->validatePayment();