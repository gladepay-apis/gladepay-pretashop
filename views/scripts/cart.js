class Cart {
    constructor(){
        this.user = '';
        this.merchant = '';
        this.amount = '';
        this.currency = '';
        this.mode = '';

        this.getDetails();
    }

    getDetails()
    {
        const cart = document.forms.gladepay_form;
        this.user = cart.user.value;
        this.merchant = cart.merchant.value;
        this.amount = cart.amount.value;
        this.currency = cart.currency.value;
        this.mode = cart.mode.value;
    }
}