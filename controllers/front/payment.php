<?php
/*  */
require (_PS_MODULE_DIR_.Configuration::get('GLADEPAY_MOD_NAME').'/src/GladeCore.php');

/*  */
class GladepayPaymentModuleFrontController extends ModuleFrontController
{
    /*  */
    protected $successReference;

    /*  */
    public function postProcess()
    {
        $cart = $this->context->cart;
        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'gladepay') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->module->l('This payment method is not available.', 'validation'));
        }

        $this->context->smarty->assign([
            'params' => $_REQUEST,
        ]);

        //$this->setTemplate('payment_return.tpl');
        $this->setTemplate('module:gladepay/views/templates/front/payment_return.tpl');


        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

        $currency = $this->context->currency;
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $extraVars = array();

        if (!$this->processPayment())
        {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $this->module->validateOrder($cart->id, Configuration::get('PS_OS_PAYMENT'), $total, $this->module->displayName, NULL, $extraVars, (int)$currency->id, false, $customer->secure_key);
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
    }

    public function processPayment()
    {
        $returnValue = false;

        $merchant = [];
        $merchant['id'] = Configuration::get('GLADEPAY_MERCHANT_ID');
        $merchant['key'] = Configuration::get('GLADEPAY_MERCHANT_KEY');

        $mode = Tools::getValue('mode');
        $txnref =  Tools::getValue('reference');
        $otp =  Tools::getValue('otp');

        Configuration::updateValue('GLADE_TRANSACTION_REFERENCE', $txnref);
        Configuration::updateValue('GLADE_TRANSACTION_OTP', $otp);


        if(!empty($otp) && !empty($txnref))
        {
            //
            $gladePayment = new \App\GladeCore();
            $gladePayment->setValidationParameters($mode, $merchant, $txnref, $otp);

            //
            if ($gladePayment->validatePayment() && $gladePayment->getResponse('status') == 200)
            {
                if ($gladePayment->verifyPayment() && $gladePayment->getResponse('status') == 200)
                {
                    $this->successReference = $gladePayment->getResponse('ref');
                    $returnValue = true;
                }
            }
        }


        return $returnValue;
    }
}