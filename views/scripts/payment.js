/*

 */
class GladePaymentHandler {
    constructor(cardObject, cartObject)
    {
        this.card = cardObject;
        this.cart = cartObject;
        this.requestString = '';
    }
    prepareRequestString()
    {
        this.requestString += 'card_number=' + this.card.number;
        this.requestString += '&exp_month=' + this.card.exp_month;
        this.requestString += '&exp_year=' + this.card.exp_year;
        this.requestString += '&cvc=' + this.card.cvc;
        this.requestString += '&pin=' + this.card.pin;
        this.requestString += '&user=' + this.cart.user;
        this.requestString += '&merchant=' + this.cart.merchant;
        this.requestString += '&amount=' + this.cart.amount;
        this.requestString += '&currency=' + this.cart.currency;
        this.requestString += '&mode=' + this.cart.mode;

    }
    getRequestString()
    {
        this.prepareRequestString();

        return this.requestString;
    }
    generateOTP()
    {
        const system = new System();
        const xhr = system.createAjaxObject();
        let responseText;

        xhr.open("POST", "modules/gladepay/controllers/front/generate_otp.php");
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(this.getRequestString());

        xhr.addEventListener('readystatechange', function () {
            if(xhr.readyState === 4)
            {
                localStorage.setItem('otp_response', xhr.responseText);
            }
        });
    }
}