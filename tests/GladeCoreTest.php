<?php
/**
 * Created by PhpStorm.
 * User: nathan
 * Date: 12/3/18
 * Time: 1:22 AM
 */

require __DIR__ . '/../src/GladeCore.php';

use PHPUnit\Framework\TestCase;

class GladeCoreTest extends TestCase
{
    protected $glade;

    public function setUp()
    {
        $merchant = [
            "id" => "GP0000001",
            "key" => "123456789"
        ];

        $card = [
            "card_no" => "5438898014560229",
            "expiry_month" => "09",
            "expiry_year" => "19",
            "ccv" => "789",
            "pin" =>"3310"
        ];

        $user = [
            "firstname" =>"John",
            "lastname" =>"Doe",
            "email" =>"hello@example.com",
            "ip" => "192.168.33.10",
            "fingerprint" => "cccvxbxbxb"
        ];
        $this->glade = new \App\GladeCore();
        $this->glade->setInitiationParameters($merchant, 14000, $card, $user, 'NG');
    }

    /** @test */
    public function check_if_payment_is_initiated()
    {

        $this->assertTrue($this->glade->initiatePayment());
        $this->assertEquals('202', $this->glade->getResponse('status'));
    }

    /** @test */
    public function check_if_payment_is_validated()
    {
        $this->assertTrue($this->glade->validatePayment());
        $this->assertEquals('202', $this->glade->getResponse('status'));
        $this->assertEquals('xyz', $this->glade->getResponse('txnRef'));
    }

    /** @test */
    public function check_if_payment_is_verified()
    {
        $this->assertTrue($this->glade->verifyPayment());
        $this->assertEquals('200', $this->glade->getResponse('status'));
        $this->assertEquals('xyz', $this->glade->getResponse('txnRef'));
    }
}