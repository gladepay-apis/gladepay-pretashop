# Gladepay Gateway for PrestaShop 1.7

The Gladepay Gateway for Prestashop is a module that allows customers to make payments using Gladepay on Prestashop.

## Installation

1. Download the [gladepay.zip](https://gitlab.com/gladepay-apis/gladepay-pretashop/blob/master/gladepay.zip) file

2. In your PrestaShop back office, click the Modules link on the left side menu of your dashboard.

![screenshot](step1.png)

3. Click the Upload A Module button. A dialog box will pop up, asking you to upload a file. Drag and drop the zip file into the dialog box or click the ‘Select file’ optiion.

![screenshot](step2.png)

![screenshot](step3.png)

4. If you are not automatically redirected on successful installation, manually navigate to the Installed Modules tab then click 'Configure' on the GladePay module.

![screenshot](step4.png)

5. You will be directed to an interface where you can input your API Credentials. These Credentials are available on your GladePay dashboard. (Sign Up at (https://gladepay.com) for your credentials)
![screenshot](step5.png)

6. Make yourself some Money!

