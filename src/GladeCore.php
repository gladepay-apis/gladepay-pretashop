<?php
/**
 * Created by PhpStorm.
 * User: nathan
 * Date: 12/3/18
 * Time: 12:35 AM
 */

namespace App;


use function ICanBoogie\array_flatten;
use PhpParser\Node\Expr\Array_;

class GladeCore
{
    private $demo_uri = "https://demo.api.gladepay.com/payment";
    private  $live_uri = "https://api.gladepay.com/payment";

    protected $request_uri = "";
    private $request_data = '';
    protected $request;

    private $merchant = [];
    private $card = [];
    private $user = [];

    private $amount;
    private $country;
    const CURRENCY = 'NGN';

    private $otp;

    private $response = [];
    private $error;

    public function getResponse($key = '')
    {
        $returnValue;

        switch (strtolower($key))
        {
            case 'status':
                $returnValue = $this->response['status'];
                break;
            case 'txnref':
                $returnValue = $this->response['txnRef'];
                break;
            default:
                $returnValue = $this->response;
        }

        return $returnValue;
    }
    public function setInitiationParameters($merchant, $amount, $card, $user , $mode = '', $country = '')
    {
        $this->request_uri = $this->live_uri;

        $this->merchant['id'] = isset($merchant['id']) ? $merchant['id'] : 0;
        $this->merchant['key'] = isset($merchant['key']) ? $merchant['key'] : '';

        $this->amount = $amount > 0 ? $amount : 0;
        $this->country = (isset($country) && strlen($country) === 2)? strtoupper($country) : 'NG';

        $this->card['card_no'] = (isset($card['number']) && strlen($card['number']) === 16) ? $card['number'] : 0;
        $this->card['expiry_month'] = (isset($card['expiry_month']) && strlen($card['expiry_month']) == 2) ? $card['expiry_month'] : 0;
        $this->card['expiry_year'] = (isset($card['expiry_year']) && strlen($card['expiry_year']) == 2) ? $card['expiry_year'] : 0;
        $this->card['ccv'] = (isset($card['ccv']) && strlen($card['ccv']) == 3 ) ? $card['ccv'] : 0;
        $this->card['pin'] = (isset($card['pin']) && strlen($card['pin'])  >= 4 )? $card['pin'] : 0;

        $this->user['fingerprint'] = isset($user['fingerprint']) ? $user['fingerprint'] : 'cccvxbxbxb';
        $this->user['firstname'] = isset($user['firstname']) ? $user['firstname'] : '';
        $this->user['lastname'] = isset($user['lastname']) ? $user['lastname'] : '';
        $this->user['email'] = isset($user['email']) ? $user['email'] : '';
        $this->user['ip'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '192.168.33.10';



        if(strtolower($mode) == 'test')
        {
            $this->request_uri =  $this->demo_uri;

            $this->user['fingerprint'] = 'cccvxbxbxb';
            $this->user['ip'] = '192.168.33.10';
        }

    }

    public function setValidationParameters($mode = '', $merchant, $txnRef, $otp)
    {
        $this->request_uri = (!empty(mode) && strtolower($mode) == 'test') ? $this->demo_uri: $this->live_uri;
        $this->merchant = isset($merchant) ? $merchant : [];
        $this->response['txnRef'] = (isset($txnRef) && !empty($txnRef)) ? $txnRef : '';
        $this->otp = isset($otp) ? $otp : '';
    }

    private function buildInitiationString()
    {
        $string = [
            "action" => "initiate",
            "paymentType" => "card",
            "user" => $this->user,
            "card" => $this->card,
            "amount" => $this->amount,
            "country" => $this->country,
            "currency" => self::CURRENCY
        ];

        $this->request_data = json_encode($string);
    }

    private function buildValidationString()
    {
        $string = [
            "action" => "validate",
            "txnRef" => $this->response['txnRef'],
            "otp" => $this->otp
        ];
        
        $this->request_data = json_encode($string);
    }

    private function buildVerificationString()
    {
        $string = [
            "action" => "verify",
            "txnRef" => $this->response['txnRef']
        ];

        $this->request_data = json_encode($string);
    }

    private function prepareRequest()
    {
        $this->request = curl_init();

        curl_setopt_array($this->request, array(
            CURLOPT_URL => $this->request_uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $this->request_data,
            CURLOPT_HTTPHEADER => array(
                "key: " . $this->merchant['key'],
                "mid: " . $this->merchant['id']
            ),
        ));
    }

    /*

    */
    private function sendRequest()
    {
        /*  Set Return value    */
        $returnValue = true;

        /*  Make request    */
        $response = curl_exec($this->request);
        $error = curl_error($this->request);

        curl_close($this->request);

        if ($error) {
            //  If request fails
            $this->error = $error;
            $returnValue = false;
        } else {
            //  If request is successful
            $this->response = (array) json_decode($response);
        }

        return $returnValue;
    }

    /*
     *  This initiates a payment
    */
    public function initiatePayment()
    {
        // default return value
        $returnValue = false;

        //
        $this->buildInitiationString();
        $this->prepareRequest();
        if($this->sendRequest()) {
            $returnValue = true;
        }

        return $returnValue;
    }

    /*
     * This validates a payment
     */
    public function validatePayment()
    {
        // default return value
        $returnValue = false;

        //
        $this->buildValidationString();
        $this->prepareRequest();
        if($this->sendRequest()) {
            $returnValue = true;
        }

        return $returnValue;
    }

    /*
     * This validates a payment
     */
    public function verifyPayment()
    {
        // default return value
        $returnValue = false;

        //
        $this->buildVerificationString();
        $this->prepareRequest();
        if($this->sendRequest()) {
            $returnValue = true;
        }

        return $returnValue;
    }
}