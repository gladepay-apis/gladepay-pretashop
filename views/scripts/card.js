
class Card {
    constructor()
    {
        this.number = 0;
        this.exp_month = 1;
        this.exp_year = 12;
        this.cvc = 123;
        this.pin = 0;

        this.getDetails();
    }

    getDetails()
    {
        let card_info_form = document.forms.gladepay_form;
        this.number = card_info_form.card_number.value;
        this.exp_month = card_info_form.card_expiry_month.value;

        this.exp_year = card_info_form.card_expiry_year.value;
        this.exp_year = this.exp_year[2] + '' +this.exp_year[3];

        this.cvc = card_info_form.card_cvc.value;
        this.pin = card_info_form.card_pin.value;
    }
}
