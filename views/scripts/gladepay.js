/*  */

window.addEventListener('load', function(){
    const genOtPBtn = document.getElementById('get_otp_btn') ? document.getElementById('get_otp_btn') : null;
    const resendOTPBtn = document.getElementById('resend_otp_btn') ? document.getElementById('resend_otp_btn'): null;

    function getOTP() {
        const card = new Card();
        const cart = new Cart();
        const gladePayment = new GladePaymentHandler(card, cart);

        gladePayment.generateOTP();
    };
    
    function processOTP() {
        /* Get Form sections */
        const card_info = document.getElementById('card_info');
        const card_otp = document.getElementById('card_otp');
        const card_gp_loader = document.getElementById('gp_loader');

        /*  Check if form is filled correctly   */
        const card = new Card();
        const system = new System();
        if(card.number.length !== 16 || card.cvc.length !== 3 || card.pin.length < 4)
        {
            system.displayFormMessage('form-message', 'Invalid Card details', 3);
            return;
        }

        card_info.style.display = 'none';
        card_otp.style.display = 'none';
        card_gp_loader.style.display = "block";
        setTimeout(function (){
            getOTP();

            try {
                //
                const glade_form = document.forms.gladepay_form;
                const otp_response = JSON.parse(localStorage.getItem('otp_response'));

                if (otp_response.status == 202)
                {
                    glade_form.reference.value = otp_response.txnRef;
                    card_gp_loader.style.display = "none";
                    card_otp.style.display = "block";
                    system.displayFormMessage('form-message', 'OTP generated successfully', 1);
                }else
                {
                    system.displayFormMessage('form-message', 'Oops! Please try again', 2);
                    card_gp_loader.style.display = "none";
                    card_info.style.display = "block";
                }

                console.log(otp_response);
            } catch (err) {
                console.log(err);
                card_gp_loader.style.display = "none";
                card_info.style.display = "block";

                localStorage.removeItem('otp_response');
            }

            localStorage.removeItem('otp_response');
        }, 2000);
    };

    if (genOtPBtn !== null && resendOTPBtn !== null)
    {

        genOtPBtn.addEventListener('click', function () {
            processOTP();
        });

        resendOTPBtn.addEventListener('click', function () {
            processOTP();
        })
    }
});